package edu.ucsd.cs110s.temperature; 
public class Fahrenheit extends Temperature 
{ 
 public Fahrenheit(float t) 
 { 
 super(t); 
 } 
 public String toString() 
 { 
   return " " + this.getValue();
 
 }
@Override
public Temperature toCelsius() {
	float v = getValue(); 
	v = v - 32; 
	v = (float)v * ((float)(5/9));  
	Temperature newTemp = new Celsius(v);
	return newTemp;
}
@Override
public Temperature toFahrenheit() {
  float v = getValue();
  Temperature newTemp = new Fahrenheit(v);
  return newTemp;
}
} 

