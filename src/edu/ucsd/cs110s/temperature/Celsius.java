package edu.ucsd.cs110s.temperature;
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{

		return "" + this.getValue();
	}

@Override
public Temperature toCelsius() {
	float v = getValue();  
	Temperature newTemp = new Celsius(v);
	return newTemp;
}
@Override
public Temperature toFahrenheit() {
	float v = getValue(); 
	v = v * ((float)(9/5));
	v = (float)v + (float)32;
	Temperature newTemp = new Fahrenheit(v);
	return newTemp;
}
}
